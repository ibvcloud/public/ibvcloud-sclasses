public without sharing class SObjectService {

    private static Set<String> exceptionValues = new Set<String>{
            'id','isdeleted','createddate','systemmodstamp','lastmodifiedbyid','createdbyid','lastmodifieddate'
    };

    private static Map<Operation,Map<SObjectType,List<String>>> cachedRestrictedFields =
            new Map<Operation,Map<SObjectType,List<String>>>();

    public enum Operation {
        OP_INSERT,OP_UPDATE,OP_DELETE,OP_UPSERT
    }

    // CRUD/FLS-safe DML operations. These honor the CRUD and FLS permissions of the running user
    public static List<sObject> insertAsUser(sObject obj) {
        return performDMLOperation(new List<sObject>{obj},Operation.OP_INSERT);
    }
    public static List<sObject> updateAsUser(sObject obj) {
        return performDMLOperation(new List<sObject>{obj},Operation.OP_UPDATE);
    }
    public static List<sObject> upsertAsUser(sObject obj) {
        return performDMLOperation(new List<sObject>{obj},Operation.OP_UPSERT);
    }
    public static List<sObject> deleteAsUser(sObject obj) {
        return performDMLOperation(new List<sObject>{obj},Operation.OP_DELETE);
    }
    public static List<sObject> insertAsUser(List<SObject> objList) {
        return performDMLOperation(objList,Operation.OP_INSERT);
    }
    public static List<sObject> updateAsUser(List<SObject> objList) {
        return performDMLOperation(objList,Operation.OP_UPDATE);
    }
    public static List<sObject> upsertAsUser(List<SObject> objList) {
        return performDMLOperation(objList,Operation.OP_UPSERT);
    }
    public static List<sObject> deleteAsUser(List<SObject> objList) {
        return performDMLOperation(objList,Operation.OP_DELETE);
    }

    // Pass-thru methods to raw DML operations.
    // Use these sparingly, and only with good reason, since the DML operations are not CRUD/FLS safe

    public static List<sObject> insertAsSystem(List<SObject> objList) {
        insert objList;
        return objList;
    }
    public static List<sObject> updateAsSystem(List<SObject> objList) {
        update objList;
        return objList;
    }
    public static List<sObject> upsertAsSystem(List<SObject> objList) {
        upsert objList;
        return objList;
    }
    public static List<sObject> deleteAsSystem(List<SObject> objList) {
        delete objList;
        return new List<sObject>();
    }

    // Custom Exception Classes
    public virtual class DMLManagerException extends Exception {
        public SObjectType objType {get; private set;}
        public Operation op{get; private set;}
    }

    public class CRUDException extends DMLManagerException {
        public CRUDException(SObjectType objType, Operation op){
            this('Access Denied: ' + op + ' on ' + objType);
            this.objType = objType;
            this.op = op;
        }
    }

    public class FLSException extends DMLManagerException {
        public SObjectField field{get; private set;}
        public FLSException(SObjectType objType, SObjectField field, Operation op) {
            this('Access Denied: ' + op + ' on ' + objType + '.' + field);
            this.objType = objType;
            this.op = op;
            this.field = field;
        }
    }

    /**
    *   objList - list of objects that should be processed.
    *   dmlOperation - dml operation type.
    *   Method validates data and process it if possible.
    */
    private static List<sObject> performDMLOperation(List<SObject> objList, Operation dmlOperation) {
        Map<SObjectType,List<Id>> objTypeMap = analyzeDMLCollection(objList, dmlOperation);
        checkCRUDPermission(objTypeMap.keySet(),dmlOperation);
        if(dmlOperation == Operation.OP_INSERT) {
            for(SObject obj : objList) {
                checkCreateAction(obj);
            }
        }
        else if (dmlOperation == Operation.OP_UPDATE || dmlOperation == Operation.OP_UPSERT) {
            Map<Id,SObject> existingRecords = getExistingRecords(objTypeMap);
            for(SObject obj : objList) {
                SObject existingRecord = existingRecords.get(obj.Id);
                if(obj.id != null) {
                    checkUpdateAction(obj,existingRecord);
                } else {
                    checkCreateAction(obj);
                }
            }
        }
        // If no errors have been thrown to this point, execute the dml operation.
        if(dmlOperation == Operation.OP_INSERT) {
            insert objList;
            return objList;
        } else if (dmlOperation == Operation.OP_UPDATE) {
            update objList;
            return objList;
        } else if (dmlOperation == Operation.OP_UPSERT) {
            upsertCollection(objList);
            return objList;
        } else if (dmlOperation == Operation.OP_DELETE) {
            delete objList;

        }
        return new List<sObject>();
    }

    /**
    *   objList - list of objects that should be processed.
    *   Method upserts collection of objects.
    *   If there is only one record in the list we should upsert it as singel record to avoid errors.
    */
    private static void upsertCollection(List<SObject> objList) {
        // This is to deal with a call to upsertAsUser with a singular object.
        // Since we wrap that into a List<SObject> (which can't be passed into an upsert)
        // we unpack it and upsert the object individually.
        if(objList.size() == 1) {
            upsert objList.get(0);
        } else {
            upsert objList;
        }
    }

    /**
    *   objList - list of objects that should be processed.
    *   Method gets actual fields present in object.  This serialization technique removes implicit nulls.
    */
    private static Map<String,Object> getFieldMapFromExistingSObject(SObject obj) {
        String s = JSON.serialize(obj);
        Map<String,Object> fieldsMap = (Map<String,Object>) JSON.deserializeUntyped(s);
        fieldsMap.remove('attributes');
        return fieldsMap;
    }

    /**
    *   obj - objcet record.
    *   Method checks if we can insert record, if not throws new custom (FLSException) Exception.
    */
    private static void checkCreateAction(SObject obj) {
        List<String> restrictedFields = cachedRestrictedFields.get(Operation.OP_INSERT).get(obj.getSObjectType());
        //Save ourselves a trip through the loop below if there are no restricted fields
        if(restrictedFields == null || restrictedFields.isEmpty()) {
            return;
        }
        Map<String,Object> fieldsMap = getFieldMapFromExistingSObject(obj);
        // If any restricted fields are present, throw an exception
        for(String fieldName : restrictedFields) {
            if(fieldsMap.get(fieldName) != null) {
                // if any of the restricted fields are present in the candidate, throw an exception
                throw new FLSException(
                        obj.getSObjectType(),
                        obj.getSObjectType().getDescribe().fields.getMap().get(fieldName),
                        Operation.OP_INSERT
                );
            }
        }
    }

    /**
    *   obj - objcet record with new data.
    *   existingRecord - object record with old data.
    *   Method checks if we can update record, if not throws new custom (FLSException) Exception.
    *   If existingRecord was not found new custom (DMLManagerException) Exception will be thrown.
    */
    private static void checkUpdateAction(SObject obj, SObject existingRecord) {
        List<String> restrictedFields = cachedRestrictedFields.get(Operation.OP_UPDATE).get(obj.getSObjectType());
        //Save ourselves a trip through the loop below if there are no restricted fields
        if(restrictedFields == null || restrictedFields.isEmpty()) {
            return;
        }
        if(existingRecord == null) {
            throw new DMLManagerException(
                    'DMLManager ERROR:  An existing record could not be found for object with Id = ' + obj.Id
            );
        }
        Map<String,Object> fieldsMap = getFieldMapFromExistingSObject(obj);
        // If any of the restricted values are present and have changed in the dml candidate object, throw an exception
        for(String fieldName : restrictedFields) {
            if(
                    existingRecord.get(fieldName) != null &&
                    fieldsMap.get(fieldName) != null &&
                    fieldsMap.get(fieldName) != existingRecord.get(fieldName)
            )
            {
                throw new FLSException(
                        obj.getSObjectType(),
                        obj.getSObjectType().getDescribe().fields.getMap().get(fieldName),
                        Operation.OP_UPDATE
                );
            }
        }
    }


    // For update and upsert operations, retrieve a Map of all existing records, for each object that has an ID.
    // objects without an Id are skipped, because there is no existing record in the database.
    private static Map<Id,SObject> getExistingRecords(Map<SObjectType,List<Id>> objTypeMap) {
        Map<ID, SObject> result = new Map<Id,SObject>();
        Map<SObjectType,List<String>> operationRestrictedFields = cachedRestrictedFields.get(Operation.OP_UPDATE);
        for(SObjectType objType : objTypeMap.keySet()) {
            List<String> restrictedFields = operationRestrictedFields.get(objType);
            List<Id> seenIds = objTypeMap.get(objType);
            String fieldList = String.join(restrictedFields,',');
            if(restrictedFields == null || restrictedFields.isEmpty()) {
                continue;
            }
            if(seenIds.isEmpty()){
                continue;
            }
            result.putAll(
                    (Database.query(
                            'SELECT ' + fieldList + ' FROM ' + objType.getDescribe().getName() + ' WHERE Id IN :seenIds')
                    )
            );
        }
        return result;
    }

    // Check CRUD permissions for the current user on the object
    private static void checkCRUDPermission(Set<SObjectType> objTypeList, Operation dmlOperation) {
        for(SObjectType objType : objTypeList) {
            DescribeSObjectResult describeObject = objType.getDescribe();
            if((dmlOperation == Operation.OP_INSERT && !describeObject.isCreateable()) ||
                    (dmlOperation == Operation.OP_UPDATE && !describeObject.isUpdateable()) ||
                    (dmlOperation == Operation.OP_DELETE && !describeObject.isDeletable()) ||
                    (dmlOperation == Operation.OP_UPSERT && !(describeObject.isCreateable() && describeObject.isUpdateable()))) {
                throw new CRUDException(objType,dmlOperation);
            }
        }
    }

    // Get a Map of all the object types in the dml request and the list of fields for each
    // that the current user cannot update, based on FLS security settings
    private static Map<SObjectType,List<Id>> analyzeDMLCollection(List<SObject> objList, Operation dmlOperation) {
        Map<SObjectType,List<Id>> result = new Map<SObjectType,List<Id>>();
        for(SObject obj : objList) {
            ensureRestrictedFieldsEntry(obj, dmlOperation);
            List<Id> seenIds = result.get(obj.getSObjectType());
            if(seenIds == null) {
                seenIds = new List<Id>();
                result.put(obj.getSObjectType(),seenIds);
            }
            if(obj.Id == null) {
                continue;
            }
            seenIds.add(obj.Id);
        }
        return result;
    }

    private static void ensureRestrictedFieldsEntry(SObject obj, Operation dmlOperation) {
        if(dmlOperation == Operation.OP_UPSERT) {
            ensureRestrictedFields(obj,Operation.OP_INSERT);
            ensureRestrictedFields(obj,Operation.OP_UPDATE);
        } else {
            ensureRestrictedFields(obj,dmlOperation);
        }
    }

    private static void ensureRestrictedFields(SObject obj, Operation dmlOperation){
        Map<SObjectType,List<String>> operationRestrictedFields = cachedRestrictedFields.get(dmlOperation);
        if(operationRestrictedFields == null){
            operationRestrictedFields = new Map<SObjectType,List<String>>();
            cachedRestrictedFields.put(dmlOperation,operationRestrictedFields);
        }
        if(!operationRestrictedFields.containsKey(obj.getSObjectType())) {
            DescribeSObjectResult describeObject = obj.getSObjectType().getDescribe();
            Map<String, Schema.SObjectField> objectFields = describeObject.fields.getMap();
            List<String> restrictedFields = new List<String>();
            for(String nm : objectFields.keyset()){
                if(!exceptionValues.contains(nm.toLowerCase())) {
                    DescribeFieldResult fr = objectFields.get(nm).getDescribe();
                    if((!fr.isCalculated()) && ((dmlOperation == Operation.OP_INSERT && !fr.isCreateable()) ||
                            (dmlOperation == Operation.OP_UPDATE && !fr.isUpdateable()))
                            ){
                        restrictedFields.add(fr.getName());
                    }  // there is not an isDeletable method at the field level
                }
            }
            operationRestrictedFields.put(obj.getSObjectType(),restrictedFields);
        }
    }
}