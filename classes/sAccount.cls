public with sharing class sAccount extends sObjectAbstract {

    private String getsObjectTypeAPIName() {
        return 'Account';
    }

    private List<String> getBaseFields() {
        return new List<String>{
                'Id',
                'Name',
                'BillingCity',
                'BillingAddress',
                'BillingLatitude',
                'BillingLongitude'
        };
    }

    public List<Account> getRecordById(Id recordId) {
        if(String.isBlank(recordId)){
            return new List<Account>();
        }
        return this.getRecordsByIds(new List<Id>{recordId}) ;
    }

    public List<Account> getRecordsByIds(List<Id> recordIds) {
        this.queryWhereConditions = this.createInCondition('Id', recordIds);
        return this.queryData();
    }

}