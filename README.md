# Salesforce sObject Util (SOQL framework)

Salesforce sObject Util provides a secure, fast, flexible and dynamic way of building a SOQL query on the Salesforce platform. 

## Why Salesforce sObject Util

Salesforce sObject Util is made to improve the flexibility of the code and enhance the productivity of the development. With Salesforce sObject Util it's easy to construct SOQL query in a bug-free and structural way. So it is a good thing to use Salesforce sObject Util when you want to request any data from Salesforce database using SOQL.

## Features

- sObject Util uses object-oriented programming paradigm and function chaining

- Supports complex queries including parent/child relationships, and nested conditions in a flexible way

- Prevents SOQL injctions

- Allows to use Aggregate functions

## Examples

### Step-by-step guide

1. First of all include **sObjectManager.cls**, **sObjectAnalyser.cls**,  **sObjectAbstract.cls**,  **sObjectQueryAnalyser**  and **sObjectAbstractException.cls** from ```src``` folder into your Org, and you are ready to go.

2. Create ```sClass``` for sObject that you plan to query using this template (example for 'Account' object):

```java 
sAccount.cls

public with sharing class sAccount extends sObjectAbstract {
	private String getsObjectTypeAPIName(){
		return 'Account';
	}
	
	private List<String> getBaseFields(){
		return new List<String>{
				'Id',
				'Name'
		};
	}
	
	public List<sObject> getRecordById(Id recordId){
		this.queryWhereConditions = ' Id =\''+recordId+'\'';
		return this.queryData();
	}
}
```
---
You should notice few important thing during creation of new ```sClass```:

- Name should be ```s+sObject name```, in example it's ```sAccount```. If it's custom object (ObjectName__c) do not include __c in a name, it should be like ```sObjectNameCustom```. "Custom" part is used to protect system from situation when someone has created Custom Object with the same API name as standard, for exmpale ```Opportunity``` and ```Opportunity__c```, or ```Task``` and ```Task__c```.

- New ```sClass``` should extend ```sObjectAbstract``` class;

- New ```sClass``` should implement ```getsObjectTypeAPIName```, this method should return API name of ```sObject```, in our example it is Account, if it were custom object it would look like ```CustomObject__c```;

- New ```sClass``` should implement ```getBaseFields```, this fileds will be queryed for every SOQL query. If you need to query some rare fields usethis.additionalFields varible.
---
3. Now, when ```sClass``` is ready you should create new query method. If class already exist check if non of existing query methods can be used by you instead of creating new one. In our example query method it's ```getRecordById```.

4. After query method is ready you can use it in your code anywhere you needed. Like in this example:
```java 
sClass usage

sAccount accUtil = (sAccount) sObjectManager.getObjectManager('Account');
List<Account> myList = accUtil.getRecordById('some_record_Id');
```
5. If you run exact same query in one run time process you will recive cached results instead of quring from database. In case you still want to query from database you can turn off ache for this operation. Turn off cache at all is strongly not recomended.

```java 
sAccount accUtil = (sAccount) sObjectManager.getObjectManager('Account');
accUtil.skipCacheOnce = true;
//accUtil.skipCache = true;
List<Account> myList = accUtil.getRecordById('some_record_Id');
```
6. Before making query you can check if records that you are looking for have been already queried and cached. For this you should run next code:

```java 
Analyser analyserInst = Analyser.getInstance();
Map<Id, sObject> recordsById = analyserInst.getCachedRecordsByType('Account'); //Use correct sObject API Name,for custom object it will be CustomObject__c
```
7. If you want to check if your query was already fired and results are there, you can use next code:

```java 
Analyser analyserInst = Analyser.getInstance();
String soqlQuery = 'Select Id from Account';
List<sObject> records = analyserInst.getCachedResults('Account', soqlQuery); //Use correct sObject API Name, for custom object it will be CustomObject__c
```
Use this in ```sClass queryMethods``` to check if query has been fired already.

---
#### ⓘ  this.queryData() Method always return list of redcords.

sObjectManager will always return singel instance of sClass to ensure that all transactions process by this class cashed and tracked.

```skipCache = true``` - will applay for all queries from this sClass, so if you have

```java 
sAccount accUtil = (sAccount) sObjectManager.getObjectManager('Account');
```
and someone in other part of runtime will have another ```sClass``` for the same ```sObject```

```java 
sAccount accUtil2 = (sAccount) sObjectManager.getObjectManager('Account');
```

```skipCache = true``` will applay for accUtil as well for ```accUtil2```.

That is why you should not use ```skipCache = true```, if you need to skip cache use ```skipCacheOnce``` instead.


## Some more useful examples

This is example of sOpportunity class, lets take a look on method that we have here:


**getSearchRecords** - is a complex method that can search for an Opportunity with given name, but also filtered by its Parent Id(account).

To use List of values in query we will use ```String.join(List<String>, '\',\'')```

For search string always use ```String.escapeSingleQuotes(String)``` method, as search string usually are user input.

```java 
sOpportunity.cls

public with sharing class sOpportunity extends sObjectAbstract {

	private String getsObjectTypeAPIName() {
		return 'Opportunity';
	}
	
	private List<String> getBaseFields() {
		return new List<String>{
				'Id',
				'Probability',
				'StageName',
				'Name',
				'Amount',
				'CloseDate',
				'Account.Name',
				'LastModifiedDate',
				'Description',
				'AccountId'
		};
	}
	
	public List<Opportunity> getSearchRecords(String parentRecordId, List<String> excludeIds, String searchString) {
		queryWhereConditions = ' Name LIKE \'%' + String.escapeSingleQuotes(searchString) + '%\' ';
			if (String.isNotBlank(parentRecordId)) {
				if (String.isNotBlank(queryWhereConditions)) {
					queryWhereConditions += ' AND AccountId = \'' + parentRecordId + '\' ';
				} else {
					queryWhereConditions = ' AccountId = \'' + parentRecordId + '\' ';
				}
			}
			if (!excludeIds.isEmpty()) {
				String soqlListString = '\'' + String.join(excludeIds, '\',\'') + '\'';
				if (String.isNotBlank(queryWhereConditions)) {
					queryWhereConditions += ' AND Id NOT IN (' + soqlListString + ')';
				} else {
					queryWhereConditions = ' Id NOT IN (' + soqlListString + ')';
				}
			}
		return queryData();
	}
	
}
```

In this class you can find example of adding rare fields and also ```subquery```.

Pay attention to ```this.clearAdditionalFields```. Anyway even if u setup ```clearAdditionalFields = false```, and added some filed twice, system will add it only once to avoid SOQL erorrs.

**```!IMPORTANT```** - Do not add ```addiitionalFields``` or ```subquery``` if it's not absolutely necessary. Remember most fileds should be mantioned in ```getBaseFields``` method.
**```!IMPORTANT```** - Do not add change ```this.clearAdditionalFields``` to false, if it's not absolutely necessary.

```java
sAccount.cls
 
public with sharing class sAccount extends sObjectAbstract {
	private String getsObjectTypeAPIName(){
		return 'Account';
	}
	
	private List<String> getBaseFields(){
		return new List<String>{
				'Id',
				'Name'
		};
	}
	
	public List<sObject> getRecordById(Id recordId){
		this.queryWhereConditions = ' Id =\''+recordId+'\'';
		return this.queryData();
	}
	
	public List<sObject> getRecordByIdWithCustomFields(Id recordId){
		this.additionalFields.add('OwnerId');
		this.queryWhereConditions = ' Id =\''+recordId+'\'';
		return this.queryData();
	}
	
	public List<sObject> getRecordByIdWithSubQuery(Id recordId){
		this.additionalFields.add('(SELECT Id, AccountId, CaseNumber FROM Cases)');
		//Base on this.clearAdditionalFields that by default is true.
		//this.additionalFields.add('OwnerId');
		this.queryWhereConditions = ' Id =\''+recordId+'\'';
		return this.queryData();
	}
	
}
/**
sAccount accUtil = (sAccount) sObjectManager.getObjectManager('Account');
system.debug(accUtil.getRecordById('some_record_Id'));
system.debug(accUtil.getRecordByIdWithCustomFields('some_record_Id'));
system.debug(accUtil.getRecordByIdWithSubQuery('some_record_Id')[0].getSObjects('Cases'));
system.debug(accUtil.getRecordByIdWithSubQuery('some_record_Id')[0].getSObjects('Cases'));
accUtil.clearAdditionalFields = false;
system.debug(accUtil.getRecordById('some_record_Id'));
system.debug(accUtil.getRecordByIdWithCustomFields('some_record_Id'));
system.debug(accUtil.getRecordByIdWithSubQuery('some_record_Id')[0].getSObjects('Cases'));
system.debug(accUtil.getRecordByIdWithSubQuery('some_record_Id')[0].getSObjects('Cases'));
*/
```

## Aggregate function example

This is example of ```sOpportunity``` class, lets take a look on method that we have here:
```java
public List<AggregateResult> aggregateTclOpportunity(Set<Id> accountIds) {
	this.skipBaseField = true;
	this.additionalFields = new List<String> {'AccountId','Count(Id)'};
	List<String> accountIdsList = new List<String>();
	for (Id accountId : accountIds) {
		accountIdsList.add('\'' + accountId + '\'');
	}
	String joinedAccountIds = String.join(accountIdsList, ',');
	this.queryWhereConditions = 'StageName = \'\' AND AccountId IN (' + joinedAccountIds + ') AND Sale_Point_City__c = \'\'';
	this.queryPostConditions = 'GROUP BY AccountId LIMIT 5000';
	return queryData();
}
```